var myDictionary = angular.module('myDictionary', []);

myDictionary.controller('DictionaryController', function ($scope, $http) {
    $http.get('http://mydictionaryapi.azurewebsites.net/api/definitions')
        .success(function (data) {
            $scope.phrases = data;
        });
});