﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyDictionary.UI.Startup))]
namespace MyDictionary.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
