using MyDictionary.Models;

namespace MyDictionary.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MyDictionary.Models.MyDictionaryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MyDictionary.Models.MyDictionaryContext context)
        {
            context.Definitions.AddOrUpdate(x => x.DefinitionId,
                new Definition { DefinitionId = 1, Word = "Polyglot ", Explanation = "Knowing or using several languages." },
                new Definition { DefinitionId = 2, Word = "Computer", Explanation = "an electronic device for storing and processing data, typically in binary form, according to instructions given to it in a variable program." },
                new Definition { DefinitionId = 3, Word = "Consider ", Explanation = "At the moment, artemisinin-based therapies are considered the best treatment, but cost about $10 per dose - far too much for impoverished communities." }
            );
        }
    }
}
