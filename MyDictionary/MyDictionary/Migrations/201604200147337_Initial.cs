namespace MyDictionary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Definitions",
                c => new
                    {
                        DefinitionId = c.Int(nullable: false, identity: true),
                        Word = c.String(nullable: false),
                        Explanation = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.DefinitionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Definitions");
        }
    }
}
