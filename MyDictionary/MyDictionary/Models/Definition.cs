using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyDictionary.Models
{
    public class Definition
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DefinitionId { get; set; }
        [Required]
        public string Word { get; set; }
        [Required]
        public string Explanation { get; set; }
    }
}