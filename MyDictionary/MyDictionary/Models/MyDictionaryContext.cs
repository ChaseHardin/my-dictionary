﻿using System.Data.Entity;

namespace MyDictionary.Models
{
    public class MyDictionaryContext : DbContext
    {
        public MyDictionaryContext() : base("name=MyDictionaryContext")
        {
        }

        public System.Data.Entity.DbSet<MyDictionary.Models.Definition> Definitions { get; set; }
    }
}