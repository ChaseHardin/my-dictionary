﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MyDictionary.Models;

namespace MyDictionary.Controllers
{
    public class DefinitionsController : ApiController
    {
        private MyDictionaryContext db = new MyDictionaryContext();

        // GET: api/Definitions
        public IQueryable<Definition> GetDefinitions()
        {
            return db.Definitions;
        }

        // GET: api/Definitions/5
        [ResponseType(typeof(Definition))]
        public async Task<IHttpActionResult> GetDefinition(int id)
        {
            Definition definition = await db.Definitions.FindAsync(id);
            if (definition == null)
            {
                return NotFound();
            }

            return Ok(definition);
        }

        // PUT: api/Definitions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDefinition(int id, Definition definition)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != definition.DefinitionId)
            {
                return BadRequest();
            }

            db.Entry(definition).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DefinitionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Definitions
        [ResponseType(typeof(Definition))]
        public async Task<IHttpActionResult> PostDefinition(Definition definition)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Definitions.Add(definition);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = definition.DefinitionId }, definition);
        }

        // DELETE: api/Definitions/5
        [ResponseType(typeof(Definition))]
        public async Task<IHttpActionResult> DeleteDefinition(int id)
        {
            Definition definition = await db.Definitions.FindAsync(id);
            if (definition == null)
            {
                return NotFound();
            }

            db.Definitions.Remove(definition);
            await db.SaveChangesAsync();

            return Ok(definition);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DefinitionExists(int id)
        {
            return db.Definitions.Count(e => e.DefinitionId == id) > 0;
        }
    }
}